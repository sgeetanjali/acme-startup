/**
* @author ACME Banking
* @description This class is test class for GetCoreBankingAccountsCtrl.
* <p /><p /> 
*/
@isTest(seeAllData = false)
public class GetCoreBankingAccountsCtrlTest {
    
    /**
    * description Method to rest getAccounts.
    */
    public static testMethod void testGetAccountsSuccess() {
        List<Account> lstAccounts = new List<Account>();
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());            
        lstAccounts = GetCoreBankingAccountsCtrl.getListOfBankingAccounts('searchString');
        Test.stopTest();
        System.assert(lstAccounts.size() > 0 , 'Error fetching data.');
    }
    
    /**
    * description Method to rest getAccounts.
    */
    public static testMethod void testGetAccountsFailure() {
        List<Account> lstAccounts = new List<Account>();
        Test.StartTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());            
        lstAccounts = CoreBankingCalloutHandler.callGetAccount('failureScenario');
        Test.stopTest();
        System.assert(lstAccounts.size() == 0 , 'Error fetching data.');
    }
}