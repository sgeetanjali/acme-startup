/**
* @author ACME Banking
* @description This class is used for static Constants across the org.
* <p /><p /> 
*/
public class GlobalConstants {
    
    public static final String HTTP_METHOD_GET = 'GET';
    public static final Integer GET_STATUS_CODE = 200;
    
}