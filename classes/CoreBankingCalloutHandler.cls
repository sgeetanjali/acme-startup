/**
* @author ACME Banking
* @description This class is used connect with ACME Banking org.
* <p /><p /> 
*/
public class CoreBankingCalloutHandler {
    
    /** 
    *  @description  Method used to make rest call to ACME banking Org to fecth account data.
    *  @param        strSearchKey  Search string to search against name of Banking accounts.
    *  @return       List<Account> - List of account banking records.
    */
    public static List<Account> callGetAccount(String strSearchKey)
    {
        Http objHttp = new Http();
        HttpRequest objRequest = new HttpRequest();
        HttpResponse  objResponse;
        List<Account> lstAccount = new List<Account>();
        try{
            objRequest.setMethod(GlobalConstants.HTTP_METHOD_GET);
            objRequest.setEndpoint('callout:Core_Banking/services/apexrest/getAccounts/'+strSearchKey);            
            objResponse = objHttp.send(objRequest);  
            if(objResponse.getStatusCode() == GlobalConstants.GET_STATUS_CODE) {
                if(String.isNotBlank(objResponse.getBody())){
                    lstAccount = (List<Account>) JSON.deserialize(objResponse.getBody(), List<Account>.class);
                }
            }else{
                //Placeholder to make a entry into error log object
            }            
        }
        catch(Exception objException){            
            //Placeholder to make a entry into error log object
             throw new AuraHandledException(objException.getMessage());
        }
        return lstAccount;
    }
}