/**
* @author ACME Banking
* @description MockService for Core Banking.
* <p /><p /> 
*/
@isTest
public class MockHttpResponseGenerator implements HttpCalloutMock {
    
    /**
    * description Simulates the Web Service response for the test class.
    * @param objRequest HTTP request.
    * @return HTTP Response.
    */
    public HTTPResponse respond(HTTPRequest objRequest) {
        HttpResponse objResponse = new HttpResponse();
        List<Account> lstAccounts = new List<Account>();        
        lstAccounts.add(new Account(Name='TestAccount'));        
        String endpoint = objRequest.getEndpoint();
        if (endpoint.contains('searchString')){
            objResponse.setHeader('Content-Type', 'application/json');
            objResponse.setBody(JSON.serialize(lstAccounts));
            objResponse.setStatusCode(200);
        }
        else if(endpoint.contains('failureScenario')){
            objResponse.setStatusCode(500);
        }
        return objResponse;
    }
}