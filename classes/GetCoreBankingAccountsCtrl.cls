/**
* @author ACME Banking
* @description This class is used fetch account records from ACME banking.
* <p /><p /> 
*/
public with sharing class GetCoreBankingAccountsCtrl {
    
    /** 
    *  @description  Method used fetch ACME banking account data.
    *  @param        strSearchKey  Search string to search against name of Banking accounts.
    *  @return       List<Account> - List of account banking records.
    */
    @AuraEnabled	
    public static List<Account> getListOfBankingAccounts(String strSearchKey){
        strSearchKey = String.isBlank(strSearchKey) ? '' : String.escapeSingleQuotes(strSearchKey);
        return CoreBankingCalloutHandler.callGetAccount(strSearchKey);
    }
}