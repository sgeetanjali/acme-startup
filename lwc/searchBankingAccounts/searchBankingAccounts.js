/**
* This file is used as a controller for account search.
* <p /><p />
* @author ACME Banking
*/
import { LightningElement, track } from 'lwc';

//Import custom lables
import strNoDataMessage from "@salesforce/label/c.No_Results_Message";
import strNoData from "@salesforce/label/c.No_Data_Message";
import strSearchError from "@salesforce/label/c.Search_Error_Message";

//Import apex
import fetchBankingAccounts from "@salesforce/apex/GetCoreBankingAccountsCtrl.getListOfBankingAccounts";

const columns = [
    { label: 'Account Name', fieldName: 'accountName', type: 'text' },
    { label: 'Billing Country', fieldName: 'billingCountry', type: 'text' },
    { label: 'No Of Contacts', fieldName: 'contacts', type: 'number' },
    { label: 'No Of Opportunities', fieldName: 'opportunities', type: 'number' },
];

export default class SearchBankingAccounts extends LightningElement {

    @track strSearchString;
    @track boolDisbaleSearch = true;
    @track lstData = [];    
    @track boolShowResult = false;
    @track strErrorMessage = strNoData;
    intMinSearchLength = 3;
    lstColumns = columns;

    /**
     * @author ACME Banking 
     * @description Handler for on change for member search.
     */
    handleChange(objEvent) {
        this.strSearchString = objEvent.target.value;
        if (this.strSearchString.length >= this.intMinSearchLength) {
            this.boolDisbaleSearch = false;
        } else {
            this.boolDisbaleSearch = true;
        }
    }

    /**
     * @author ACME Banking 
     * @description Handler for on change for member search.
     */
    handleAccountSearch() {
        this.boolShowResult = false;
        fetchBankingAccounts({
            strSearchKey: this.strSearchString
        }).then((objResult) => {
            if (objResult && objResult.length > 0) {
                this.formatResult(objResult);
                this.boolShowResult = true;
            } else {
                this.boolShowResult = false;
                this.strErrorMessage = strNoDataMessage;
            }
        }).catch((objError) => {
            this.boolShowResult = false;
            this.strErrorMessage = strSearchError;
        });
    }

    /**
     * @author ACME Banking 
     * @description Format account data to view on UI.
     */
    formatResult(result) {
        let lstAccounts = [];
        let objAccount = {};
        result.forEach(objResult => {
            objAccount = {};
            objAccount.Id = objResult.Id;
            objAccount.accountName = objResult.Name;
            objAccount.billingCountry = objResult.BillingCity;
            objAccount.contacts = objResult.Contacts ? objResult.Contacts.length : 0;
            objAccount.opportunities = objResult.Opportunities ? objResult.Opportunities.length : 0;
            lstAccounts.push(objAccount);
        });
        this.lstData = lstAccounts;
    }
}